# Tests

## Test simple transation from Russian to English

| Action | Result |
| - | - |
| Go to `https://translate.yandex.ru` | ✅ |
| Pass the captcha if needed | ✅ |
| Click on the area for translation, cursor should appear | ✅ |
| Put into the box text "Привет мир!" | ✅ |
| The text should be translated to "Hello world!" | ✅ |


## Test simple transation from Russian to Spanish

| Action | Result |
| - | - |
| Go to `https://translate.yandex.ru` | ✅ |
| Pass the captcha if needed | ✅ |
| Click on the language on the right, list of languages should appear | ✅ |
| Choose the spanish language | ✅ |
| Click on the area for translation, cursor should appear | ✅ |
| Put into the box text "Привет мир!" | ✅ |
| The text should be translated to "Hola mundo!" | ✅ |

## Test swap languages

| Action | Result |
| - | - |
| Go to `https://translate.yandex.ru` | ✅ |
| Pass the captcha if needed | ✅ |
| Click on the area for translation, cursor should appear | ✅ |
| Put into the box text "Привет мир!" | ✅ |
| The text should be translated to "Hello world!" | ✅ |
| Click on the "swap" button | ✅ |
| Texts should be swapped | ✅ |