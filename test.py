import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

options = webdriver.FirefoxOptions()
options.add_argument('--width=1920')
options.add_argument('--height=1080')

driver = webdriver.Remote(command_executor="http://selenium:4444/wd/hub", options=options)
time.sleep(1)
driver.get("https://translate.yandex.ru")

try:
    time.sleep(2)
    elem = driver.find_element(By.CLASS_NAME, "CheckboxCaptcha-Button")
    elem.click()
except:
    pass

time.sleep(3)
elem = driver.find_element(By.ID, "textarea")
print(elem.text)
elem.click()
elem.clear()
elem.send_keys("Привет мир!")

time.sleep(5)

elem = driver.find_element(By.ID, "translation")
assert elem.text == "Hello world!"

driver.close()
